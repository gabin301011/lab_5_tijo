package pl.edu.pwsztar.domain.mapper;

@FunctionalInterface
public interface ConvertInterface<F, T> {
    T convert(F from);
}
